package agenda.telefonica;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcos Junior
 */
public class Amigo extends Contato{
    private List<Reuniao> listaDeReunioes;
    Amigo(){
        super.nascimento = new Data();
        super.endereco = new Endereco();
        super.listaDeTelefones = new ArrayList<>();
        super.listaDeEmails = new ArrayList<>();
        listaDeReunioes = new ArrayList<>();
    }

    public List<Reuniao> getListaDeReunioes() {
        return listaDeReunioes;
    }

    public void setListaDeReunioes(List<Reuniao> listaDeReunioes) {
        this.listaDeReunioes = listaDeReunioes;
    }
    
}
