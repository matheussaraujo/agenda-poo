package agenda.telefonica;

import java.util.ArrayList;

/**
 * @author Marcos Junior
 */
public class Familia extends Contato{
    private String parentesco;

    Familia(){
        super.nascimento = new Data();
        super.endereco = new Endereco();
        super.listaDeTelefones = new ArrayList<>();
        super.listaDeEmails = new ArrayList<>();
    }
    
    public String getParentesco() {
        return parentesco;
    }
    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }
    
}
