package agenda.telefonica;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcos Junior
 */
public class Trabalho extends Contato{
    private String empresa;
    private String setor;
    private List<Reuniao> listaDeReunioes;
    Trabalho(){
        super.nascimento = new Data();
        super.endereco = new Endereco();
        super.listaDeTelefones = new ArrayList<>();
        super.listaDeEmails = new ArrayList<>();
        listaDeReunioes = new ArrayList<>();
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public List<Reuniao> getListaDeReunioes() {
        return listaDeReunioes;
    }

    public void setListaDeReunioes(List<Reuniao> listaDeReunioes) {
        this.listaDeReunioes = listaDeReunioes;
    }
    
}
