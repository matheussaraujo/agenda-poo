package agenda.telefonica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Marcos Junior
 */
public class Data {
    int dia;
    int mes;
    int ano;
    
   Data(){}
   Data(int dia, int mes, int ano){
       this.dia = dia;
       this.mes = mes;
       this.ano = ano;
   }
   Data(Date date){
       Calendar c = new GregorianCalendar();
       c.setTime(date);
       this.dia = c.get(Calendar.DAY_OF_MONTH);
       this.mes = c.get(Calendar.MONTH)+1;
       this.ano = c.get(Calendar.YEAR);
   }
   Data(String data){
       SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
       try {
       Date date = dataFormater.parse(data);
       Calendar c = new GregorianCalendar();
       c.setTime(date); 
       this.dia = c.get(Calendar.DAY_OF_MONTH);
       this.mes = c.get(Calendar.MONTH)+1;
       this.ano = c.get(Calendar.YEAR);
       } catch (ParseException e) {e.printStackTrace(); }
    }
  
   public int contaDias(Data data){
       long dias = 0;
       try {
            SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
            Date data1 = dataFormater.parse(dia + "/" + mes + "/" + ano);
            Date data2 = dataFormater.parse(data.dia + "/" + data.mes + "/" + data.ano);
            dias = ((((data2.getTime() / 1000) / 60) / 60) / 24)
                  -((((data1.getTime() / 1000) / 60) / 60) / 24);
        } catch (ParseException e) { e.printStackTrace(); }
       return (int)dias;
   }
   public int contaAnos(Data data){
       long anos = 0;
       try {
            SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
            Date data1 = dataFormater.parse(dia + "/" + mes + "/" + ano);
            Date data2 = dataFormater.parse(data.dia + "/" + data.mes + "/" + data.ano);
            anos = ((((((data2.getTime() / 1000) / 60) / 60) / 24) / 30) / 12)
                  -((((((data1.getTime() / 1000) / 60) / 60) / 24) / 30) / 12);
        } catch (ParseException e) { e.printStackTrace(); }
       return (int)anos;
   }
    @Override
   public String toString(){
        return dia+"/"+mes+"/"+ano;
   }
   public Date toDate(){
       SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
       Date date = null;
       try {
            date = dataFormater.parse(dia+"/"+(mes-1)+"/"+ano);
       } catch (ParseException e) {e.printStackTrace(); }
       return date;
   }
}
