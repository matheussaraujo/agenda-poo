package agenda.telefonica;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.JOptionPane;

/**
 * @author Marcos Junior
 */
public class Agenda extends javax.swing.JFrame {

    List<Contato> contatosList = new ArrayList<>();
    String[] contatosString;
    JavaDB BancoDeDados;
    //----------------------------------------------------------------------------------
    int indiceJLReunioes;
    //**indiceJLReunioes->necessario pra atualizar o indice da lista reuniões, porque nao da pra setar
    // o indiceJLReunioes de dentro do item changed, e perde qual evento foi selecionado depois de 
    //mudar o model da lista,para exibir a mensagem da reunião selecionada
    //-----------------------------------------------------------------------------------

    String funcao;

    public Agenda() {
        initComponents();
        BancoDeDados = new JavaDB();
        FrameAdicionarContato.setLocation(jLabel3.getLocation());
        atualizaContatos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FrameAdicionarContato = new javax.swing.JFrame();
        jLabel2 = new javax.swing.JLabel();
        addcontatoETnome = new javax.swing.JTextField();
        addcontatoCBgrupo = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        addcontatoCBdia = new javax.swing.JComboBox<>();
        addcontatoCBmes = new javax.swing.JComboBox<>();
        addcontatoCBano = new javax.swing.JComboBox<>();
        addcontatoETpais = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        addcontatoETestado = new javax.swing.JTextField();
        addcontatoETcidade = new javax.swing.JTextField();
        addcontatoETbairro = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        addcontatoETrua = new javax.swing.JTextField();
        addcontatoETnumero = new javax.swing.JTextField();
        addcontatoETcomplemento = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        LBaddcontatoEmpresa = new javax.swing.JLabel();
        addcontatoETempresa_parentesco = new javax.swing.JTextField();
        addcontatoETsetor = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        addcontatoBTsalvar = new javax.swing.JButton();
        addcontatoETcep = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        FrameAdicionarTelefone = new javax.swing.JFrame();
        addtelefoneCBoperadora = new javax.swing.JComboBox<>();
        addtelefoneETcodigo = new javax.swing.JTextField();
        addtelefoneETnumero = new javax.swing.JTextField();
        addtelefoneBTadicionar = new javax.swing.JButton();
        FrameAdicionarReuniao = new javax.swing.JFrame();
        jLabel7 = new javax.swing.JLabel();
        addreuniaoETtitulo = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        addreuniaoCBdia = new javax.swing.JComboBox<>();
        addreuniaoCBmes = new javax.swing.JComboBox<>();
        addreuniaoCBano = new javax.swing.JComboBox<>();
        addreuniaoBTsalvar = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        addreuniaoETdescricao = new javax.swing.JTextArea();
        FrameAdicionarEmail = new javax.swing.JFrame();
        jLabel16 = new javax.swing.JLabel();
        adicionaremailETemail = new javax.swing.JTextField();
        adicionaremailBTsalvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JLcontatos = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        LBnomeContato = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        LBendereco1 = new javax.swing.JLabel();
        LBendereco2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        BTadicionarContato = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        LBdataNascimento = new javax.swing.JLabel();
        LBaniversario1 = new javax.swing.JLabel();
        LBcontaDias = new javax.swing.JLabel();
        LBaniversario2 = new javax.swing.JLabel();
        LBidade = new javax.swing.JLabel();
        LBaniversario3 = new javax.swing.JLabel();
        BTeditarContato = new javax.swing.JButton();
        BTexcluirContato = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        LBgrupo = new javax.swing.JLabel();
        LBinfo1 = new javax.swing.JLabel();
        LBinfo2 = new javax.swing.JLabel();
        BTadicionarReuniao = new javax.swing.JButton();
        CBmodoExibicao = new javax.swing.JComboBox<>();
        BTadicionarTelefone = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        JLtelefones = new javax.swing.JList<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        JLreunioes = new javax.swing.JList<>();
        BTexcluirTelefone = new javax.swing.JButton();
        BTeditarTelefone = new javax.swing.JButton();
        BTexcluirReuniao = new javax.swing.JButton();
        BTeditarReuniao = new javax.swing.JButton();
        ETbusca = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        JLemails = new javax.swing.JList<>();
        BTeditarEmail = new javax.swing.JButton();
        BTexcluirEmail = new javax.swing.JButton();
        BTadicionarEmail = new javax.swing.JButton();

        FrameAdicionarContato.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        FrameAdicionarContato.setTitle("Adicionar Contato");
        FrameAdicionarContato.setMinimumSize(new java.awt.Dimension(235, 300));
        FrameAdicionarContato.setPreferredSize(new java.awt.Dimension(220, 300));
        FrameAdicionarContato.setResizable(false);
        FrameAdicionarContato.setSize(new java.awt.Dimension(220, 329));
        FrameAdicionarContato.setType(java.awt.Window.Type.UTILITY);

        jLabel2.setText("Nome:");

        addcontatoCBgrupo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Amigo", "Família", "Trabalho" }));
        addcontatoCBgrupo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                addcontatoCBgrupoItemStateChanged(evt);
            }
        });

        jLabel6.setText("Grupo:");

        jLabel8.setText("Nascimento:");

        addcontatoCBdia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        addcontatoCBmes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));

        addcontatoCBano.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017" }));

        jLabel10.setText("País:");

        jLabel14.setText("Estado:");

        jLabel15.setText("Cidade:");

        jLabel17.setText("Bairro:");

        jLabel18.setText("Rua:");

        jLabel19.setText("Numero:");

        jLabel20.setText("Complemento:");

        LBaddcontatoEmpresa.setText("Empresa:");

        jLabel22.setText("Setor:");

        addcontatoBTsalvar.setText("Salvar");
        addcontatoBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addcontatoBTsalvarActionPerformed(evt);
            }
        });

        jLabel21.setText("CEP:");

        javax.swing.GroupLayout FrameAdicionarContatoLayout = new javax.swing.GroupLayout(FrameAdicionarContato.getContentPane());
        FrameAdicionarContato.getContentPane().setLayout(FrameAdicionarContatoLayout);
        FrameAdicionarContatoLayout.setHorizontalGroup(
            FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETsetor, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(LBaddcontatoEmpresa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoETempresa_parentesco))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoBTsalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoETcep))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETcomplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETrua, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETbairro, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETcidade, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETestado, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETpais, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(6, 6, 6)
                        .addComponent(addcontatoETnome, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        FrameAdicionarContatoLayout.setVerticalGroup(
            FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(addcontatoETnome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(addcontatoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addcontatoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addcontatoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETpais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETbairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETrua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcomplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoBTsalvar)
                    .addComponent(addcontatoCBgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETempresa_parentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LBaddcontatoEmpresa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETsetor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FrameAdicionarContato.getAccessibleContext().setAccessibleParent(BTadicionarContato);

        FrameAdicionarTelefone.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        FrameAdicionarTelefone.setTitle("Adicionar Telefone");
        FrameAdicionarTelefone.setMinimumSize(new java.awt.Dimension(241, 100));
        FrameAdicionarTelefone.setResizable(false);
        FrameAdicionarTelefone.setSize(new java.awt.Dimension(241, 100));
        FrameAdicionarTelefone.setType(java.awt.Window.Type.UTILITY);

        addtelefoneCBoperadora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TIM", "VIVO", "OI", "CLARO", "GVT", "NEXTEL" }));

        addtelefoneBTadicionar.setText("Adicionar");
        addtelefoneBTadicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtelefoneBTadicionarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FrameAdicionarTelefoneLayout = new javax.swing.GroupLayout(FrameAdicionarTelefone.getContentPane());
        FrameAdicionarTelefone.getContentPane().setLayout(FrameAdicionarTelefoneLayout);
        FrameAdicionarTelefoneLayout.setHorizontalGroup(
            FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                        .addComponent(addtelefoneCBoperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(addtelefoneETcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(addtelefoneETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(addtelefoneBTadicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FrameAdicionarTelefoneLayout.setVerticalGroup(
            FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addtelefoneCBoperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addtelefoneETcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addtelefoneETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(addtelefoneBTadicionar)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        FrameAdicionarReuniao.setTitle("Salvar Reunião");
        FrameAdicionarReuniao.setMinimumSize(new java.awt.Dimension(250, 210));
        FrameAdicionarReuniao.setResizable(false);
        FrameAdicionarReuniao.setSize(new java.awt.Dimension(250, 210));
        FrameAdicionarReuniao.setType(java.awt.Window.Type.UTILITY);

        jLabel7.setText("Titulo:");

        jLabel9.setText("Data:");

        addreuniaoCBdia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        addreuniaoCBmes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));

        addreuniaoCBano.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032" }));

        addreuniaoBTsalvar.setText("Salvar");
        addreuniaoBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addreuniaoBTsalvarActionPerformed(evt);
            }
        });

        addreuniaoETdescricao.setColumns(20);
        addreuniaoETdescricao.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        addreuniaoETdescricao.setRows(5);
        addreuniaoETdescricao.setText("Descrição:");
        jScrollPane6.setViewportView(addreuniaoETdescricao);

        javax.swing.GroupLayout FrameAdicionarReuniaoLayout = new javax.swing.GroupLayout(FrameAdicionarReuniao.getContentPane());
        FrameAdicionarReuniao.getContentPane().setLayout(FrameAdicionarReuniaoLayout);
        FrameAdicionarReuniaoLayout.setHorizontalGroup(
            FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                        .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                                .addComponent(addreuniaoCBdia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addreuniaoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addreuniaoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(addreuniaoETtitulo)))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FrameAdicionarReuniaoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(addreuniaoBTsalvar)))
                .addContainerGap())
        );
        FrameAdicionarReuniaoLayout.setVerticalGroup(
            FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(addreuniaoETtitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addreuniaoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(addreuniaoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addreuniaoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addreuniaoBTsalvar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FrameAdicionarEmail.setTitle("Adicionar Email");
        FrameAdicionarEmail.setMinimumSize(new java.awt.Dimension(311, 65));
        FrameAdicionarEmail.setResizable(false);
        FrameAdicionarEmail.setSize(new java.awt.Dimension(311, 65));

        jLabel16.setText("Email:");

        adicionaremailBTsalvar.setText("Salvar");
        adicionaremailBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionaremailBTsalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FrameAdicionarEmailLayout = new javax.swing.GroupLayout(FrameAdicionarEmail.getContentPane());
        FrameAdicionarEmail.getContentPane().setLayout(FrameAdicionarEmailLayout);
        FrameAdicionarEmailLayout.setHorizontalGroup(
            FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarEmailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adicionaremailETemail, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adicionaremailBTsalvar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FrameAdicionarEmailLayout.setVerticalGroup(
            FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarEmailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(adicionaremailETemail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(adicionaremailBTsalvar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(744, 565));
        setSize(new java.awt.Dimension(744, 565));

        JLcontatos.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Sem contatos..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLcontatos.setToolTipText("Lista de Contatos");
        JLcontatos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLcontatosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(JLcontatos);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("CONTATOS");

        LBnomeContato.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LBnomeContato.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LBnomeContato.setText("Nenhum contato selecionado");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_telefones.png"))); // NOI18N
        jLabel3.setText("Telefones:");

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_contatos.png"))); // NOI18N

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_contato.png"))); // NOI18N

        LBendereco1.setText("Rua - N° 000 - Bairro - Complemento");

        LBendereco2.setText("Cidade - Estado - País - CEP");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_endereco.png"))); // NOI18N
        jLabel4.setText("Endereço:");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        BTadicionarContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarcontato.png"))); // NOI18N
        BTadicionarContato.setText("Adicionar Contato");
        BTadicionarContato.setToolTipText("Adicionar Contato");
        BTadicionarContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarContatoActionPerformed(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_aniversario.png"))); // NOI18N
        jLabel5.setText("Aniversário:");

        LBcontaDias.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        BTeditarContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editarcontato.png"))); // NOI18N
        BTeditarContato.setToolTipText("Editar Contato");
        BTeditarContato.setEnabled(false);
        BTeditarContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarContatoActionPerformed(evt);
            }
        });

        BTexcluirContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_excluircontato.png"))); // NOI18N
        BTexcluirContato.setToolTipText("Excluir Contato");
        BTexcluirContato.setEnabled(false);
        BTexcluirContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirContatoActionPerformed(evt);
            }
        });

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_familia.png"))); // NOI18N
        LBgrupo.setText("Família");

        BTadicionarReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarReuniao.setToolTipText("Adicionar Reunião");
        BTadicionarReuniao.setEnabled(false);
        BTadicionarReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarReuniaoActionPerformed(evt);
            }
        });

        CBmodoExibicao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exibir Todos", "Exibir Família", "Exibir Amigos", "Exibir Trabalho" }));
        CBmodoExibicao.setToolTipText("Selecionar por Grupo");
        CBmodoExibicao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CBmodoExibicaoItemStateChanged(evt);
            }
        });

        BTadicionarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarTelefone.setToolTipText("Adicionar Telefone");
        BTadicionarTelefone.setEnabled(false);
        BTadicionarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarTelefoneActionPerformed(evt);
            }
        });

        JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhum telefone salvo..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLtelefones.setToolTipText("Lista de Telefones");
        JLtelefones.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLtelefonesValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(JLtelefones);

        JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhuma reunião cadastrada..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLreunioes.setToolTipText("Lista de Reuniões");
        JLreunioes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                JLreunioesMouseReleased(evt);
            }
        });
        JLreunioes.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLreunioesValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(JLreunioes);

        BTexcluirTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirTelefone.setToolTipText("Excluir Telefone");
        BTexcluirTelefone.setEnabled(false);
        BTexcluirTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirTelefoneActionPerformed(evt);
            }
        });

        BTeditarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarTelefone.setToolTipText("Editar Telefone");
        BTeditarTelefone.setEnabled(false);
        BTeditarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarTelefoneActionPerformed(evt);
            }
        });

        BTexcluirReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirReuniao.setToolTipText("Excluir Reunião");
        BTexcluirReuniao.setEnabled(false);
        BTexcluirReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirReuniaoActionPerformed(evt);
            }
        });

        BTeditarReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarReuniao.setToolTipText("Editar Reunião");
        BTeditarReuniao.setEnabled(false);
        BTeditarReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarReuniaoActionPerformed(evt);
            }
        });

        ETbusca.setText("Busca...");
        ETbusca.setToolTipText("Nome, número, email, ddd ou operadora.");
        ETbusca.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ETbuscaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                ETbuscaFocusLost(evt);
            }
        });
        ETbusca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ETbuscaKeyReleased(evt);
            }
        });

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_email.png"))); // NOI18N
        jLabel11.setText("Emails:");

        JLemails.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhum email cadastrado..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLemails.setToolTipText("Lista de Emails");
        JLemails.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLemailsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(JLemails);

        BTeditarEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarEmail.setToolTipText("Editar Email");
        BTeditarEmail.setEnabled(false);
        BTeditarEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarEmailActionPerformed(evt);
            }
        });

        BTexcluirEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirEmail.setToolTipText("Excluir Email");
        BTexcluirEmail.setEnabled(false);
        BTexcluirEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirEmailActionPerformed(evt);
            }
        });

        BTadicionarEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarEmail.setToolTipText("Adicionar Email");
        BTadicionarEmail.setEnabled(false);
        BTadicionarEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarEmailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(CBmodoExibicao, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETbusca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BTadicionarContato, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LBnomeContato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTeditarContato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTexcluirContato))
                    .addComponent(jSeparator2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTadicionarTelefone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTeditarTelefone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTexcluirTelefone))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BTadicionarEmail)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BTeditarEmail)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BTexcluirEmail))))
                        .addGap(20, 20, 20)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5)
                            .addComponent(jSeparator3)
                            .addComponent(LBinfo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LBinfo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(LBgrupo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                                .addComponent(BTadicionarReuniao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTeditarReuniao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTexcluirReuniao))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(LBaniversario1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBcontaDias)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBaniversario2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBidade)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBaniversario3))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBdataNascimento)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jSeparator4)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(LBendereco1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LBendereco2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ETbusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CBmodoExibicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTadicionarContato))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(BTeditarContato)
                                .addComponent(BTexcluirContato))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(LBnomeContato, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3)
                                        .addComponent(BTeditarTelefone, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addComponent(BTadicionarTelefone)
                                    .addComponent(BTexcluirReuniao)
                                    .addComponent(BTeditarReuniao)
                                    .addComponent(BTadicionarReuniao)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(LBgrupo)
                                        .addComponent(BTexcluirTelefone)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBinfo1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBinfo2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel5)
                                            .addComponent(LBdataNascimento))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(LBaniversario1)
                                            .addComponent(LBcontaDias)
                                            .addComponent(LBaniversario2)
                                            .addComponent(LBidade)
                                            .addComponent(LBaniversario3)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTexcluirEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTeditarEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTadicionarEmail, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addGap(13, 13, 13)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jSeparator5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LBendereco1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LBendereco2)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
//SELETOR DE EXIBIÇÂO----------------------------------------------------------------------------
    private void CBmodoExibicaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CBmodoExibicaoItemStateChanged
        atualizaContatos();
        limpaTela();
    }//GEN-LAST:event_CBmodoExibicaoItemStateChanged
//SELETOR DE EXIBIÇÂO----------------------------------------------------------------------------

//EVENTOS DAS LISTAS-----------------------------------------------------------------------------
    private void JLreunioesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLreunioesValueChanged
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLreunioes.getSelectedIndex() != -1)){
                indiceJLReunioes = JLreunioes.getSelectedIndex();
                if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Trabalho){
                    Trabalho contatoT = (Trabalho) contatosList.get(JLcontatos.getSelectedIndex());
                    String titulo = contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).titulo;
                    Data data = contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data;
                    String mensagem = contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).mensagem;
                    String reunioes[] = new String[contatoT.getListaDeReunioes().size()];
                    for (int i = 0; i < contatoT.getListaDeReunioes().size(); i++) {
                        String previsao = " - Ok";
                        if(contatoT.getListaDeReunioes().get(i).data.contaDias(new Data(new Date()))<0){
                                previsao = " - daqui a "+ new Data(new Date()).contaDias(contatoT.getListaDeReunioes().get(i).data)+" dias";
                        }
                        if (i == JLreunioes.getSelectedIndex()) {
                            reunioes[i] = "<HTML>" + titulo + " - (" + data.toString() + ")" + previsao
                                    +"<BR>"
                                    + mensagem.replace("\n", "<BR>");
                        } else {
                            reunioes[i] = contatoT.getListaDeReunioes().get(i).titulo + " - ("
                                    + contatoT.getListaDeReunioes().get(i).data.toString() + ")" + previsao;
                        }
                    }
                    JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                        public int getSize() {return reunioes.length;}
                        public String getElementAt(int i) {return reunioes[i];}
                    });
                }    
                if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Amigo){
                    Amigo contatoA = (Amigo) contatosList.get(JLcontatos.getSelectedIndex());
                    String titulo = contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).titulo;
                    Data data = contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data;
                    String mensagem = contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).mensagem;
                    String reunioes[] = new String[contatoA.getListaDeReunioes().size()];
                    for (int i = 0; i < contatoA.getListaDeReunioes().size(); i++) {
                        String previsao = " - Ok";
                        if(contatoA.getListaDeReunioes().get(i).data.contaDias(new Data(new Date()))<0){
                                previsao = " - daqui a "+ new Data(new Date()).contaDias(contatoA.getListaDeReunioes().get(i).data)+" dias";
                        }
                        if (i == JLreunioes.getSelectedIndex()) {
                            reunioes[i] = "<HTML>"+titulo+" - ("+ data.toString()+")" + previsao
                                    +"<BR>"
                                    + mensagem.replace("\n", "<BR>");
                        } else {
                            reunioes[i] = contatoA.getListaDeReunioes().get(i).titulo + " - ("
                                    + contatoA.getListaDeReunioes().get(i).data + ")" + previsao;
                        }
                    }
                    JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                        public int getSize() {return reunioes.length;}
                        public String getElementAt(int i) {return reunioes[i];}
                    });
                }  
                BTeditarReuniao.setEnabled(true);
                BTexcluirReuniao.setEnabled(true);
            } else {
                BTeditarReuniao.setEnabled(false);
                BTexcluirReuniao.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLreunioesValueChanged
    private void JLtelefonesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLtelefonesValueChanged
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLtelefones.getSelectedIndex() != -1)) {
                BTeditarTelefone.setEnabled(true);
                BTexcluirTelefone.setEnabled(true);
            } else {
                BTeditarTelefone.setEnabled(false);
                BTexcluirTelefone.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLtelefonesValueChanged
    private void JLcontatosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLcontatosValueChanged
        if (evt.getValueIsAdjusting()) {
            if (JLcontatos.getSelectedIndex() != -1) {
                indiceJLReunioes = 0;
                indiceJLReunioes = 0;
                atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
                BTeditarContato.setEnabled(true);
                BTexcluirContato.setEnabled(true);
                
                if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Amigo){
                    BTadicionarReuniao.setEnabled(true);
                }
                if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Trabalho){
                    BTadicionarReuniao.setEnabled(true);
                }
                if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Familia){
                    BTadicionarReuniao.setEnabled(false);
                }
                BTadicionarTelefone.setEnabled(true);
                BTeditarTelefone.setEnabled(false);
                BTexcluirTelefone.setEnabled(false);
                BTeditarReuniao.setEnabled(false);
                BTexcluirReuniao.setEnabled(false);
                BTadicionarEmail.setEnabled(true);
                BTeditarEmail.setEnabled(false);
                BTexcluirEmail.setEnabled(false);
            } else {
                BTeditarContato.setEnabled(false);
                BTexcluirContato.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLcontatosValueChanged
    private void JLreunioesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JLreunioesMouseReleased
        JLreunioes.setSelectedIndex(indiceJLReunioes);
    }//GEN-LAST:event_JLreunioesMouseReleased
//EVENTOS DAS LISTAS-----------------------------------------------------------------------------

//BOTOES DE CONTATO------------------------------------------------------------------------------
    private void BTadicionarContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarContatoActionPerformed
        funcao = "adicionar";
        FrameAdicionarContato.setTitle("Adicionar Contato");
        FrameAdicionarContato.setVisible(true);
    }//GEN-LAST:event_BTadicionarContatoActionPerformed
    private void BTeditarContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarContatoActionPerformed
        funcao = "editar";
        Contato contato = contatosList.get(JLcontatos.getSelectedIndex());

        FrameAdicionarContato.setTitle("Editar Contato");
        addcontatoETnome.setText(contato.getNome());
        addcontatoCBdia.setSelectedItem(contato.getNascimento().dia);
        addcontatoCBmes.setSelectedItem(contato.getNascimento().mes);
        addcontatoCBano.setSelectedItem(contato.getNascimento().ano);
        addcontatoETpais.setText(contato.getEndereco().getPais());
        addcontatoETestado.setText(contato.getEndereco().getEstado());
        addcontatoETcidade.setText(contato.getEndereco().getCidade());
        addcontatoETbairro.setText(contato.getEndereco().getBairro());
        addcontatoETrua.setText(contato.getEndereco().getRua());
        addcontatoETnumero.setText(contato.getEndereco().getNumero());
        addcontatoETcomplemento.setText(contato.getEndereco().getComplemento());
        addcontatoETcep.setText(contato.getEndereco().getCep());
        addcontatoCBgrupo.setSelectedItem(contato.getGrupo());
        if (contato.getGrupo().equals("Amigo")) {
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
        }
        if (contato.getGrupo().equals("Família")) {
            LBaddcontatoEmpresa.setText("Parentesco:");
            Familia contatoF = (Familia) contatosList.get(JLcontatos.getSelectedIndex());
            addcontatoETempresa_parentesco.setText(contatoF.getParentesco());
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 354);//Família
        }
        if (contato.getGrupo().equals("Trabalho")) {
            LBaddcontatoEmpresa.setText("Empresa:");
            Trabalho contatoT = (Trabalho) contatosList.get(JLcontatos.getSelectedIndex());
            addcontatoETempresa_parentesco.setText(contatoT.getEmpresa());
            addcontatoETsetor.setText(contatoT.getSetor());
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 384);//Trabalho
        }
        FrameAdicionarContato.setVisible(true);
    }//GEN-LAST:event_BTeditarContatoActionPerformed
    private void BTexcluirContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirContatoActionPerformed
        BancoDeDados.excluirContato(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizaContatos();
        limpaTela();
    }//GEN-LAST:event_BTexcluirContatoActionPerformed
//BOTOES DE CONTATO------------------------------------------------------------------------------

//BOTOES DE TELEFONE-----------------------------------------------------------------------------
    private void BTadicionarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarTelefoneActionPerformed
        funcao = "adicionar";
        addtelefoneCBoperadora.setSelectedIndex(0);
        addtelefoneETcodigo.setText("");
        addtelefoneETnumero.setText("");
        addtelefoneBTadicionar.setText("Adicionar");
        FrameAdicionarTelefone.setTitle("Adicionar Telefone");
        FrameAdicionarTelefone.setVisible(true);
    }//GEN-LAST:event_BTadicionarTelefoneActionPerformed
    private void BTeditarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarTelefoneActionPerformed
        funcao = "editar";
        String operadora = contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().get(JLtelefones.getSelectedIndex()).operadora;
        String codigo = contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().get(JLtelefones.getSelectedIndex()).codigo;
        String numero = contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().get(JLtelefones.getSelectedIndex()).numero;
        addtelefoneCBoperadora.setSelectedItem(operadora);
        addtelefoneETcodigo.setText(codigo);
        addtelefoneETnumero.setText(numero);
        addtelefoneBTadicionar.setText("Salvar");
        FrameAdicionarTelefone.setTitle("Editar Telefone");
        FrameAdicionarTelefone.setVisible(true);

    }//GEN-LAST:event_BTeditarTelefoneActionPerformed
    private void BTexcluirTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirTelefoneActionPerformed
        contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().remove(JLtelefones.getSelectedIndex());
        BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTexcluirTelefone.setEnabled(false);
        BTeditarTelefone.setEnabled(false);
    }//GEN-LAST:event_BTexcluirTelefoneActionPerformed
//BOTOES DE TELEFONE-----------------------------------------------------------------------------

//BOTOES DE REUNIAO------------------------------------------------------------------------------    
    private void BTadicionarReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarReuniaoActionPerformed
        funcao = "adicionar";
        addreuniaoETtitulo.setText("");
        Calendar calendario = new GregorianCalendar();
        calendario.setTime(new Date());
        String ano = Integer.toString(calendario.get(Calendar.YEAR));
        addreuniaoCBdia.setSelectedIndex(calendario.get(Calendar.DATE) - 1);
        addreuniaoCBmes.setSelectedIndex(calendario.get(Calendar.MONTH));
        addreuniaoCBano.setSelectedItem(ano);
        addreuniaoETdescricao.setText("");
        addreuniaoBTsalvar.setText("Adicionar");
        FrameAdicionarReuniao.setTitle("Adicionar Reuniao");
        FrameAdicionarReuniao.setVisible(true);
    }//GEN-LAST:event_BTadicionarReuniaoActionPerformed
    private void BTeditarReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarReuniaoActionPerformed
        funcao = "editar";
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Trabalho){
            Trabalho contatoT = (Trabalho)contatosList.get(JLcontatos.getSelectedIndex());
            addreuniaoETtitulo.setText(contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).titulo);
            addreuniaoCBdia.setSelectedIndex(contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.dia);
            addreuniaoCBmes.setSelectedIndex(contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.mes);
            addreuniaoCBano.setSelectedItem(contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.ano);
            addreuniaoETdescricao.setText(contatoT.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).mensagem);
        }
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Amigo){
            Amigo contatoA = (Amigo)contatosList.get(JLcontatos.getSelectedIndex());
            addreuniaoETtitulo.setText(contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).titulo);
            addreuniaoCBdia.setSelectedIndex(contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.dia);
            addreuniaoCBmes.setSelectedIndex(contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.mes);
            addreuniaoCBano.setSelectedItem(contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).data.ano);
            addreuniaoETdescricao.setText(contatoA.getListaDeReunioes().get(JLreunioes.getSelectedIndex()).mensagem);
        }
        addreuniaoBTsalvar.setText("Editar");
        FrameAdicionarReuniao.setTitle("Editar Reuniao");
        FrameAdicionarReuniao.setVisible(true);
    }//GEN-LAST:event_BTeditarReuniaoActionPerformed
    private void BTexcluirReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirReuniaoActionPerformed
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Trabalho){
            Trabalho contatoT = (Trabalho)contatosList.get(JLcontatos.getSelectedIndex());
            contatoT.getListaDeReunioes().remove(JLreunioes.getSelectedIndex());
            BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
        }
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Amigo){
            Amigo contatoA = (Amigo)contatosList.get(JLcontatos.getSelectedIndex());
            contatoA.getListaDeReunioes().remove(JLreunioes.getSelectedIndex());
            BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
        }
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTeditarReuniao.setEnabled(false);
        BTexcluirReuniao.setEnabled(false);
    }//GEN-LAST:event_BTexcluirReuniaoActionPerformed
//BOTOES DE REUNIAO------------------------------------------------------------------------------

//FRAME ADD CONTATO------------------------------------------------------------------------------    
    private void addcontatoCBgrupoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_addcontatoCBgrupoItemStateChanged
        if (addcontatoCBgrupo.getSelectedIndex() == 0) {
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
        }
        if (addcontatoCBgrupo.getSelectedIndex() == 1) {
            LBaddcontatoEmpresa.setText("Parentesco:");
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 354);//Família
        }
        if (addcontatoCBgrupo.getSelectedIndex() == 2) {
            LBaddcontatoEmpresa.setText("Empresa:");
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 384);//Trabalho
        }
    }//GEN-LAST:event_addcontatoCBgrupoItemStateChanged
    private void addcontatoBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addcontatoBTsalvarActionPerformed
        if (addcontatoETnome.getText().trim().equals("")
                || addcontatoETpais.getText().trim().equals("")
                || addcontatoETestado.getText().trim().equals("")
                || addcontatoETcidade.getText().trim().equals("")
                || addcontatoETbairro.getText().trim().equals("")
                || addcontatoETrua.getText().trim().equals("")
                || addcontatoETnumero.getText().trim().equals("")
                || addcontatoETcomplemento.getText().trim().equals("")
                || addcontatoETcep.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos!!",
                    "Campos não preenchidos corretamente", JOptionPane.ERROR_MESSAGE);
        } else {
            if(addcontatoCBgrupo.getSelectedItem().toString().equals("Amigo")){
                Amigo contatoA = new Amigo();
                contatoA.setNome(addcontatoETnome.getText());
                contatoA.setGrupo("Amigo");
                contatoA.setAniversario(
                    Integer.parseInt(addcontatoCBdia.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBmes.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBano.getSelectedItem().toString())
                );
                contatoA.setEndereco(
                    new Endereco(
                        addcontatoETpais.getText(),
                        addcontatoETestado.getText(),
                        addcontatoETcidade.getText(),
                        addcontatoETbairro.getText(),
                        addcontatoETrua.getText(),
                        addcontatoETnumero.getText(),
                        addcontatoETcep.getText(),
                        addcontatoETcomplemento.getText()
                    )
                );
                if (funcao.equals("editar")) {
                    BancoDeDados.atualizarContato(contatosList.get(JLcontatos.getSelectedIndex()), contatoA);
                    limpaTela();
                }
                if (funcao.equals("adicionar")) {
                    BancoDeDados.inserirContato(contatoA);
                    limpaTela();
                }
            }
            if(addcontatoCBgrupo.getSelectedItem().toString().equals("Família")){
                Familia contatoF = new Familia();
                contatoF.setNome(addcontatoETnome.getText());
                contatoF.setGrupo("Família");
                contatoF.setAniversario(
                    Integer.parseInt(addcontatoCBdia.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBmes.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBano.getSelectedItem().toString())
                );
                contatoF.setEndereco(
                   new Endereco(
                    addcontatoETpais.getText(),
                    addcontatoETestado.getText(),
                    addcontatoETcidade.getText(),
                    addcontatoETbairro.getText(),
                    addcontatoETrua.getText(),
                    addcontatoETnumero.getText(),
                    addcontatoETcep.getText(),
                    addcontatoETcomplemento.getText()
                   )
                );
                contatoF.setParentesco(addcontatoETempresa_parentesco.getText());
                if (funcao.equals("editar")) {
                    BancoDeDados.atualizarContato(contatosList.get(JLcontatos.getSelectedIndex()), contatoF);
                }
                if (funcao.equals("adicionar")) {
                    BancoDeDados.inserirContato(contatoF);
                }
            }
            if(addcontatoCBgrupo.getSelectedItem().toString().equals("Trabalho")){
                Trabalho contatoT = new Trabalho();
                contatoT.setNome(addcontatoETnome.getText());
                contatoT.setGrupo("Trabalho");
                contatoT.setAniversario(
                    Integer.parseInt(addcontatoCBdia.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBmes.getSelectedItem().toString()),
                    Integer.parseInt(addcontatoCBano.getSelectedItem().toString())
                );
                contatoT.setEndereco(
                    new Endereco(
                        addcontatoETpais.getText(),
                        addcontatoETestado.getText(),
                        addcontatoETcidade.getText(),
                        addcontatoETbairro.getText(),
                        addcontatoETrua.getText(),
                        addcontatoETnumero.getText(),
                        addcontatoETcep.getText(),
                        addcontatoETcomplemento.getText()
                    )
                );
                contatoT.setEmpresa(addcontatoETempresa_parentesco.getText());
                contatoT.setSetor(addcontatoETsetor.getText());
                if (funcao.equals("editar")) {
                    BancoDeDados.atualizarContato(contatosList.get(JLcontatos.getSelectedIndex()), contatoT);
                }
                if (funcao.equals("adicionar")) {
                    BancoDeDados.inserirContato(contatoT);
                }
            }
            
            atualizaContatos();

            addcontatoETnome.setText("");
            addcontatoCBdia.setSelectedIndex(0);
            addcontatoCBmes.setSelectedIndex(0);
            addcontatoCBano.setSelectedIndex(0);
            addcontatoETpais.setText("");
            addcontatoETestado.setText("");
            addcontatoETcidade.setText("");
            addcontatoETbairro.setText("");
            addcontatoETrua.setText("");
            addcontatoETnumero.setText("");
            addcontatoETcomplemento.setText("");
            addcontatoETcep.setText("");
            addcontatoCBgrupo.setSelectedIndex(0);
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
            addcontatoETempresa_parentesco.setText("");
            addcontatoETsetor.setText("");
            FrameAdicionarContato.setVisible(false);
            
        }
    }//GEN-LAST:event_addcontatoBTsalvarActionPerformed
//FRAME ADD CONTATO------------------------------------------------------------------------------

//FRAME ADD TELEFONE-----------------------------------------------------------------------------    
    private void addtelefoneBTadicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtelefoneBTadicionarActionPerformed
        Telefone tel = new Telefone( //Cria instância de Telefone
                addtelefoneCBoperadora.getSelectedItem().toString(),//
                addtelefoneETcodigo.getText(), //
                addtelefoneETnumero.getText());                     //

        if (funcao.equals("adicionar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().add(tel);// adiciona ela no contato selecionado
            BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }
        if (funcao.equals("editar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().remove(JLtelefones.getSelectedIndex());
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeTelefones().add(tel);// adiciona ela no contato selecionado
            BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }

        FrameAdicionarTelefone.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza a tela
    }//GEN-LAST:event_addtelefoneBTadicionarActionPerformed
//FRAME ADD TELEFONE-----------------------------------------------------------------------------

//FRAME ADD REUNIAO-------------------------------------------------------------------------------    
    private void addreuniaoBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addreuniaoBTsalvarActionPerformed
        Reuniao reuniao = new Reuniao(
                addreuniaoETtitulo.getText(),
                new Data(
                    Integer.parseInt(addreuniaoCBdia.getSelectedItem().toString()),
                    Integer.parseInt(addreuniaoCBmes.getSelectedItem().toString()),
                    Integer.parseInt(addreuniaoCBano.getSelectedItem().toString())
                ),
                addreuniaoETdescricao.getText()
        );
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Trabalho){
            Trabalho contatoT = (Trabalho) contatosList.get(JLcontatos.getSelectedIndex());
            if (funcao.equals("adicionar")) {
                contatoT.getListaDeReunioes().add(reuniao);
                BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
            }
            if (funcao.equals("editar")) {
                contatoT.getListaDeReunioes().remove(JLreunioes.getSelectedIndex());
                contatoT.getListaDeReunioes().add(reuniao);
                BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
            }
        }
        
        if(contatosList.get(JLcontatos.getSelectedIndex()) instanceof Amigo){
            Amigo contatoA = (Amigo) contatosList.get(JLcontatos.getSelectedIndex());
            if (funcao.equals("adicionar")) {
                contatoA.getListaDeReunioes().add(reuniao);
                BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
            }
            if (funcao.equals("editar")) {
                contatoA.getListaDeReunioes().remove(JLreunioes.getSelectedIndex());
                contatoA.getListaDeReunioes().add(reuniao);
                BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
            }
        }
        
        FrameAdicionarReuniao.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
    }//GEN-LAST:event_addreuniaoBTsalvarActionPerformed
//FRAME ADD REUNIAO-------------------------------------------------------------------------------

//PESQUISAR CONTATO------------------------------------------------------------------------------   
    private void ETbuscaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ETbuscaKeyReleased
        contatosList = BancoDeDados.pesquisarContatosCompleto(CBmodoExibicao.getSelectedIndex(), ETbusca.getText());
        contatosString = new String[contatosList.size()];
        for (int i = 0; i < contatosList.size(); i++) {
            contatosString[i] = contatosList.get(i).getNome();
        }
        JLcontatos.setModel(new AbstractListModel<String>() {
            public int getSize() {
                return contatosString.length;
            }

            public String getElementAt(int i) {
                return contatosString[i];
            }
        });
        limpaTela();
    }//GEN-LAST:event_ETbuscaKeyReleased
    private void ETbuscaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ETbuscaFocusGained
        ETbusca.setText("");
    }//GEN-LAST:event_ETbuscaFocusGained
    private void ETbuscaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ETbuscaFocusLost
        ETbusca.setText("Buscar...");
    }//GEN-LAST:event_ETbuscaFocusLost
//PESQUISAR CONTATO------------------------------------------------------------------------------

//BOTOES DE EMAIL-----------------------------------------------------------------------------
    private void BTadicionarEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarEmailActionPerformed
        funcao = "adicionar";
        FrameAdicionarEmail.setTitle("Adicionar Email");
        FrameAdicionarEmail.setVisible(true);
    }//GEN-LAST:event_BTadicionarEmailActionPerformed
    private void BTeditarEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarEmailActionPerformed
        funcao = "editar";
        FrameAdicionarEmail.setTitle("Editar Email");
        FrameAdicionarEmail.setVisible(true);
    }//GEN-LAST:event_BTeditarEmailActionPerformed
    private void BTexcluirEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirEmailActionPerformed
        contatosList.get(JLcontatos.getSelectedIndex()).getListaDeEmails().remove(JLemails.getSelectedIndex());
        BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTeditarEmail.setEnabled(false);
        BTexcluirEmail.setEnabled(false);
    }//GEN-LAST:event_BTexcluirEmailActionPerformed
    private void adicionaremailBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionaremailBTsalvarActionPerformed
        if (funcao.equals("adicionar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeEmails().add(adicionaremailETemail.getText());// adiciona ela no contato selecionado
            BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }
        if (funcao.equals("editar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeEmails().remove(JLemails.getSelectedIndex());
            contatosList.get(JLcontatos.getSelectedIndex()).getListaDeEmails().add(adicionaremailETemail.getText());// adiciona ela no contato selecionado
            BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }

        FrameAdicionarEmail.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza a tela
    }//GEN-LAST:event_adicionaremailBTsalvarActionPerformed
    private void JLemailsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLemailsValueChanged
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLemails.getSelectedIndex() != -1)) {
                BTeditarEmail.setEnabled(true);
                BTexcluirEmail.setEnabled(true);
            } else {
                BTeditarEmail.setEnabled(false);
                BTexcluirEmail.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLemailsValueChanged
//BOTOES DE EMAIL-----------------------------------------------------------------------------

//MÉTODOS E MAIN==================================================================================  
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Agenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Agenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Agenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Agenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Agenda tela = new Agenda();
                tela.setVisible(true);
                tela.setResizable(false);
                tela.setTitle("Agenda Telefonica - MPrataSolutions");
            }
        });
    }

    public void atualizarTela(Contato contato) {
        LBnomeContato.setText(contato.getNome());
        if (contato.getGrupo().equals("Amigo")) {
            if(contato instanceof Amigo){
            Amigo contatoA = (Amigo) contatosList.get(JLcontatos.getSelectedIndex());
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_amigo.png")));
            LBgrupo.setText("Amigo");
            LBinfo1.setText("");
            LBinfo2.setText("");
            String reunioes[] = new String[contatoA.getListaDeReunioes().size()];
            for (int i = 0; i < contatoA.getListaDeReunioes().size(); i++) {
                String previsao = " - Ok";
                if(contatoA.getListaDeReunioes().get(i).data.contaDias(new Data(new Date()))<0){
                    previsao = " - daqui a "+ new Data(new Date()).contaDias(contatoA.getListaDeReunioes().get(i).data)+" dias";
                }
                reunioes[i] = contatoA.getListaDeReunioes().get(i).titulo + 
                        " - (" + contatoA.getListaDeReunioes().get(i).data.toString() + ")" + previsao;
            }
            JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {return reunioes.length;}
                public String getElementAt(int i) {return reunioes[i];}
            });
            }
        }
        if (contato.getGrupo().equals("Família")) {
            if(contato instanceof Familia){
            Familia contatoF = (Familia) contatosList.get(JLcontatos.getSelectedIndex());
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_familia.png")));
            LBgrupo.setText("Família");
            LBinfo1.setText("Parentesco: " + contatoF.getParentesco());
            LBinfo2.setText("");
            String reunioes[] = new String[1];
            reunioes[0] = "Contatos deste tipo não podem marcar reunião!";
            
            JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {return reunioes.length;}
                public String getElementAt(int i) {return reunioes[i];}
            });
            }
        }
        if (contato.getGrupo().equals("Trabalho")) {
            if(contato instanceof Trabalho){
            Trabalho contatoT = (Trabalho) contatosList.get(JLcontatos.getSelectedIndex());
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_trabalho.png")));
            LBgrupo.setText("Trabalho");
            LBinfo1.setText("Empresa: " + contatoT.getEmpresa());
            LBinfo2.setText("Setor: " + contatoT.getSetor());
            
            String reunioes[] = new String[contatoT.getListaDeReunioes().size()];
            for (int i = 0; i < contatoT.getListaDeReunioes().size(); i++) {
                String previsao = " - Ok";
                if(contatoT.getListaDeReunioes().get(i).data.contaDias(new Data(new Date()))<0){
                    previsao = " - daqui a "+ new Data(new Date()).contaDias(contatoT.getListaDeReunioes().get(i).data)+" dias";
                }
                reunioes[i] = contatoT.getListaDeReunioes().get(i).titulo 
                        +" - (" + contatoT.getListaDeReunioes().get(i).data.toString() + ")" + previsao;
            }
            JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {return reunioes.length;}
                public String getElementAt(int i) {return reunioes[i];}
            });
            }
        }
        LBdataNascimento.setText(contato.getNascimento().dia + "/" + contato.getNascimento().mes + "/" + contato.getNascimento().ano);
        LBaniversario1.setText("Faltam");
        LBaniversario2.setText("para completar");
        LBaniversario3.setText("!!!");
        LBendereco1.setText(contato.getEndereco().getRua() + " - N° " + 
                            contato.getEndereco().getNumero() + " - " + 
                            contato.getEndereco().getBairro() + " - " + 
                            contato.getEndereco().getComplemento());
        LBendereco2.setText(contato.getEndereco().getCidade() + " - " + 
                            contato.getEndereco().getEstado() + " - " + 
                            contato.getEndereco().getPais() + " - " + 
                            contato.getEndereco().getCep());

        //--------------------CALCULO DO ANIVERSÁRIO-------------------------------------//
        Data hoje = new Data(new Date());
        Data aniversario = new Data(contato.getNascimento().toString());
             aniversario.ano = hoje.ano;
        if(hoje.toDate().before(aniversario.toDate())){
            LBcontaDias.setText(hoje.contaDias(aniversario) + " Dias");
            LBidade.setText(contato.getNascimento().contaAnos(hoje) + " anos");
        }else 
        if(hoje.toDate().after(aniversario.toDate())){
            aniversario.ano = hoje.ano + 1;
            LBcontaDias.setText(hoje.contaDias(aniversario) + " Dias");
            LBidade.setText(contato.getNascimento().contaAnos(hoje)+1 + " anos");
        }else{
            aniversario.ano = hoje.ano + 1;
            LBcontaDias.setText(hoje.contaDias(aniversario) + " Dias");
            LBidade.setText(contato.getNascimento().contaAnos(hoje)+1+1 + " anos");
        }
        //FIM-------------------CALCULO DO ANIVERSÁRIO-----------------------------------//

        //----------------ATUALIZA LISTAS DE TELEFONE E EMAILS--------------------------//
        
            String telefones[] = new String[contato.getListaDeTelefones().size()];
            for (int i = 0; i < contato.getListaDeTelefones().size(); i++) {
                telefones[i] = contato.getListaDeTelefones().get(i).operadora + " - ("
                        + contato.getListaDeTelefones().get(i).codigo + ")"
                        + contato.getListaDeTelefones().get(i).numero;
            }
            JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() { return telefones.length;}
                public String getElementAt(int i) {return telefones[i];}
            });
            
            String emails[] = new String[contato.getListaDeEmails().size()];
            for (int i = 0; i < contato.getListaDeEmails().size(); i++) {
                emails[i] = contato.getListaDeEmails().get(i);
            }
            JLemails.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {return emails.length;}
                public String getElementAt(int i) { return emails[i];}
            });
        
        //FIM-------------ATUALIZA LISTAS DE TELEFONE E REUNIÃO--------------------------//
    }

    public void limpaTela() {
        LBnomeContato.setText("Nenhum contato selecionado");
        LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_amigo.png")));
        LBgrupo.setText("Amigo");
        LBinfo1.setText("");
        LBinfo2.setText("");
        LBdataNascimento.setText("");
        LBaniversario1.setText("");
        LBaniversario2.setText("");
        LBaniversario3.setText("");
        LBidade.setText("");
        LBcontaDias.setText("");
        LBendereco1.setText("");
        LBendereco2.setText("");
        BTexcluirContato.setEnabled(false);
        BTeditarContato.setEnabled(false);
        BTadicionarTelefone.setEnabled(false);
        BTadicionarReuniao.setEnabled(false);
        BTadicionarEmail.setEnabled(false);

        String telefones[] = new String[0];
        JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return telefones.length;
            }
            public String getElementAt(int i) {
                return telefones[i];
            }
        });
        String reunioes[] = new String[0];
        JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return reunioes.length;
            }

            public String getElementAt(int i) {
                return reunioes[i];
            }
        });
        String emails[] = new String[0];
        JLemails.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return emails.length;
            }

            public String getElementAt(int i) {
                return emails[i];
            }
        });

    }

    public void atualizaContatos() {
        contatosList = BancoDeDados.buscarContatos(CBmodoExibicao.getSelectedIndex());
        contatosString = new String[contatosList.size()];
        for (int i = 0; i < contatosList.size(); i++) {
            contatosString[i] = contatosList.get(i).getNome();
        }
        JLcontatos.setModel(new AbstractListModel<String>() {
            public int getSize() {
                return contatosString.length;
            }

            public String getElementAt(int i) {
                return contatosString[i];
            }
        });
    }
//MÉTODOS E MAIN==================================================================================

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTadicionarContato;
    private javax.swing.JButton BTadicionarEmail;
    private javax.swing.JButton BTadicionarReuniao;
    private javax.swing.JButton BTadicionarTelefone;
    private javax.swing.JButton BTeditarContato;
    private javax.swing.JButton BTeditarEmail;
    private javax.swing.JButton BTeditarReuniao;
    private javax.swing.JButton BTeditarTelefone;
    private javax.swing.JButton BTexcluirContato;
    private javax.swing.JButton BTexcluirEmail;
    private javax.swing.JButton BTexcluirReuniao;
    private javax.swing.JButton BTexcluirTelefone;
    private javax.swing.JComboBox<String> CBmodoExibicao;
    private javax.swing.JTextField ETbusca;
    private javax.swing.JFrame FrameAdicionarContato;
    private javax.swing.JFrame FrameAdicionarEmail;
    private javax.swing.JFrame FrameAdicionarReuniao;
    private javax.swing.JFrame FrameAdicionarTelefone;
    private javax.swing.JList<String> JLcontatos;
    private javax.swing.JList<String> JLemails;
    private javax.swing.JList<String> JLreunioes;
    private javax.swing.JList<String> JLtelefones;
    private javax.swing.JLabel LBaddcontatoEmpresa;
    private javax.swing.JLabel LBaniversario1;
    private javax.swing.JLabel LBaniversario2;
    private javax.swing.JLabel LBaniversario3;
    private javax.swing.JLabel LBcontaDias;
    private javax.swing.JLabel LBdataNascimento;
    private javax.swing.JLabel LBendereco1;
    private javax.swing.JLabel LBendereco2;
    private javax.swing.JLabel LBgrupo;
    private javax.swing.JLabel LBidade;
    private javax.swing.JLabel LBinfo1;
    private javax.swing.JLabel LBinfo2;
    private javax.swing.JLabel LBnomeContato;
    private javax.swing.JButton addcontatoBTsalvar;
    private javax.swing.JComboBox<String> addcontatoCBano;
    private javax.swing.JComboBox<String> addcontatoCBdia;
    private javax.swing.JComboBox<String> addcontatoCBgrupo;
    private javax.swing.JComboBox<String> addcontatoCBmes;
    private javax.swing.JTextField addcontatoETbairro;
    private javax.swing.JTextField addcontatoETcep;
    private javax.swing.JTextField addcontatoETcidade;
    private javax.swing.JTextField addcontatoETcomplemento;
    private javax.swing.JTextField addcontatoETempresa_parentesco;
    private javax.swing.JTextField addcontatoETestado;
    private javax.swing.JTextField addcontatoETnome;
    private javax.swing.JTextField addcontatoETnumero;
    private javax.swing.JTextField addcontatoETpais;
    private javax.swing.JTextField addcontatoETrua;
    private javax.swing.JTextField addcontatoETsetor;
    private javax.swing.JButton addreuniaoBTsalvar;
    private javax.swing.JComboBox<String> addreuniaoCBano;
    private javax.swing.JComboBox<String> addreuniaoCBdia;
    private javax.swing.JComboBox<String> addreuniaoCBmes;
    private javax.swing.JTextArea addreuniaoETdescricao;
    private javax.swing.JTextField addreuniaoETtitulo;
    private javax.swing.JButton addtelefoneBTadicionar;
    private javax.swing.JComboBox<String> addtelefoneCBoperadora;
    private javax.swing.JTextField addtelefoneETcodigo;
    private javax.swing.JTextField addtelefoneETnumero;
    private javax.swing.JButton adicionaremailBTsalvar;
    private javax.swing.JTextField adicionaremailETemail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    // End of variables declaration//GEN-END:variables
}
