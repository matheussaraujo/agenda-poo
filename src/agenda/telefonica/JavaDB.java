package agenda.telefonica;
/**
 * @author Marcos Junior
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JavaDB {
    Connection con;
    JavaDB(){
        try{
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            con = DriverManager.getConnection("jdbc:derby:ContatosPEOO","usuario","usuario");
            System.out.println("Conectado ao Banco de Dados :) !!");
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    
    public void inserirContato(Contato contato){
        Amigo contatoA = null;
        Familia contatoF = null;
        Trabalho contatoT = null; 
        if(contato instanceof Amigo){contatoA = (Amigo) contato;}
        if(contato instanceof Familia){contatoF = (Familia) contato;}
        if(contato instanceof Trabalho){contatoT = (Trabalho) contato;}
        try{
            char grupo;
            switch (contato.getGrupo()) {
                case "Trabalho":
                    grupo = 't';
                    break;
                case "Amigo":
                    grupo = 'a';
                    break;
                default:
                    grupo = 'f';
                    break;
            }
            JSONObject infoGrupo = new JSONObject();
            if(grupo == 'f'){infoGrupo.put("parentesco",contatoF.getParentesco());}
            if(grupo == 't'){
                infoGrupo.put("empresa",contatoT.getEmpresa());
                infoGrupo.put("setor",contatoT.getSetor());
            }
            
            JSONObject endereco = new JSONObject();
            endereco.put("pais", contato.getEndereco().getPais());
            endereco.put("estado", contato.getEndereco().getEstado());
            endereco.put("cidade", contato.getEndereco().getCidade());
            endereco.put("bairro", contato.getEndereco().getBairro());
            endereco.put("rua", contato.getEndereco().getRua());
            endereco.put("numero", contato.getEndereco().getNumero());
            endereco.put("cep", contato.getEndereco().getCep());
            endereco.put("complemento", contato.getEndereco().getComplemento());
            
            JSONObject nascimento = new JSONObject();
            nascimento.put("dia", contato.getNascimento().dia);
            nascimento.put("mes", contato.getNascimento().mes);
            nascimento.put("ano", contato.getNascimento().ano);
            
            
            JSONArray JlistaDeTelefones = new JSONArray();
            for(int i = 0; i< contato.getListaDeTelefones().size(); i++){
                JSONObject telefone = new JSONObject();
                telefone.put("operadora", contato.getListaDeTelefones().get(i).operadora);
                telefone.put("codigo", contato.getListaDeTelefones().get(i).codigo);
                telefone.put("numero", contato.getListaDeTelefones().get(i).numero);
                JlistaDeTelefones.add(telefone);
            }
            JSONObject telefones = new JSONObject();
            telefones.put("telefones",JlistaDeTelefones);
            JSONObject reunioes = new JSONObject();
            if(contato instanceof Trabalho){
                JSONArray JlistaDeReunioes = new JSONArray();
                for(int i = 0; i< contatoT.getListaDeReunioes().size(); i++){
                    JSONObject reuniao = new JSONObject();
                    reuniao.put("titulo", contatoT.getListaDeReunioes().get(i).titulo);
                    reuniao.put("data", contatoT.getListaDeReunioes().get(i).data.toString());
                    reuniao.put("mensagem", contatoT.getListaDeReunioes().get(i).mensagem);
                    JlistaDeReunioes.add(reuniao);
                }
                reunioes = new JSONObject();
                reunioes.put("reunioes", JlistaDeReunioes);
            }
            if(contato instanceof Amigo){
                JSONArray JlistaDeReunioes = new JSONArray();
                for(int i = 0; i< contatoA.getListaDeReunioes().size(); i++){
                    JSONObject reuniao = new JSONObject();
                    reuniao.put("titulo", contatoA.getListaDeReunioes().get(i).titulo);
                    reuniao.put("data", contatoA.getListaDeReunioes().get(i).data.toString());
                    reuniao.put("mensagem", contatoA.getListaDeReunioes().get(i).mensagem);
                    JlistaDeReunioes.add(reuniao);
                }
                reunioes = new JSONObject();
                reunioes.put("reunioes", JlistaDeReunioes);
            }
            
            JSONArray JlistaDeEmails = new JSONArray();
            for(int i = 0; i< contato.getListaDeEmails().size(); i++){
                JlistaDeEmails.add(contato.getListaDeEmails().get(i));
            }
            JSONObject emails = new JSONObject();
            emails.put("emails", JlistaDeEmails);
            
            Statement stm = con.createStatement();
            stm.execute("INSERT INTO CONTATOS VALUES("
                    + "'"+contato.getNome()+"',"
                    + "'"+grupo+"',"
                    + "'"+infoGrupo.toString()+"',"
                    + "'"+endereco.toString()+"',"
                    + "'"+nascimento.toString()+"',"
                    + "'"+telefones.toString()+"',"
                    + "'"+reunioes.toString()+"',"
                    + "'"+emails.toString()+"'" 
                    + ")");
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
    }
    public void atualizarContato(Contato contatoAntigo, Contato contatoNovo){
        excluirContato(contatoAntigo);
        inserirContato(contatoNovo);
    }
    public void excluirContato(Contato contato){
        try{
            Statement stm = con.createStatement();
            stm.execute("DELETE FROM CONTATOS WHERE NOME = '"+contato.getNome()+"'");
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public List buscarContatos(int modoExibicao){
        String where = "";
        if(modoExibicao == 1){where = " WHERE GRUPO = 'f'";}
        if(modoExibicao == 2){where = " WHERE GRUPO = 'a'";}
        if(modoExibicao == 3){where = " WHERE GRUPO = 't'";}
        
        List<Contato> Contatos = new ArrayList<>();
        try{
            
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("select * from CONTATOS"+ where +" order by NOME asc");
                      
            while(res.next()){
                String nome = res.getString(1);
                String grupo = res.getString(2);
                switch(grupo){
                    case "t":
                        grupo = "Trabalho";
                    break;
                    case "a":
                        grupo = "Amigo";
                    break;
                    case "f":
                        grupo = "Família";
                    break;
                }
                if(grupo.equals("Família")){
                    Familia contatoF = new Familia();
                    contatoF.setNome(nome);
                    contatoF.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoF.setParentesco(infoGrupo.get("parentesco").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoF.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoF.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoF.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoF.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    Contatos.add(contatoF);
                }
                if(grupo.equals("Amigo")){
                    Amigo contatoA = new Amigo();
                    contatoA.setNome(nome);
                    contatoA.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoA.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoA.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoA.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoA.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoA.getListaDeReunioes().add(reuniaoPOJO);
                    }
                    Contatos.add(contatoA);
                }
                if(grupo.equals("Trabalho")){
                    Trabalho contatoT = new Trabalho();
                    contatoT.setNome(nome);
                    contatoT.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoT.setEmpresa(infoGrupo.get("empresa").toString());
                    contatoT.setSetor(infoGrupo.get("setor").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoT.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoT.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoT.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoT.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoT.getListaDeReunioes().add(reuniaoPOJO);
                    }
                    Contatos.add(contatoT);
                }
            }
            
        }catch(SQLException  | ParseException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
        return Contatos;
    }
    public List pesquisarContatos(int modoExibicao, String pesquisa){
        String where = "";
        if(modoExibicao == 0){where = " WHERE NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 1){where = " WHERE GRUPO = 'f' AND NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 2){where = " WHERE GRUPO = 'a' AND NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 3){where = " WHERE GRUPO = 't' AND NOME LIKE '%"+pesquisa+"%'";}
        
        List<Contato> Contatos = new ArrayList<>();
        try{
            
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("select * from CONTATOS"+ where +" order by NOME asc");
                      
            while(res.next()){
                String nome = res.getString(1);
                String grupo = res.getString(2);
                switch(grupo){
                    case "t":
                        grupo = "Trabalho";
                    break;
                    case "a":
                        grupo = "Amigo";
                    break;
                    case "f":
                        grupo = "Família";
                    break;
                }
                if(grupo.equals("Trabalho")){
                    Trabalho contatoT = new Trabalho();
                    contatoT.setNome(nome);
                    contatoT.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoT.setEmpresa(infoGrupo.get("empresa").toString());
                    contatoT.setSetor(infoGrupo.get("setor").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    Endereco end = new Endereco(
                        endereco.get("pais").toString(),
                        endereco.get("estado").toString(),
                        endereco.get("cidade").toString(),
                        endereco.get("bairro").toString(),
                        endereco.get("rua").toString(),
                        endereco.get("numero").toString(),
                        endereco.get("cep").toString(),
                        endereco.get("complemento").toString()
                    );
                    contatoT.setEndereco(end);
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoT.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoT.getListaDeTelefones().add(tel);
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoT.getListaDeReunioes().add(reuniaoPOJO);
                    }              
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoT.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    Contatos.add(contatoT);
                }
                if(grupo.equals("Amigo")){
                    Amigo contatoA = new Amigo();
                    contatoA.setNome(nome);
                    contatoA.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    Endereco end = new Endereco(
                        endereco.get("pais").toString(),
                        endereco.get("estado").toString(),
                        endereco.get("cidade").toString(),
                        endereco.get("bairro").toString(),
                        endereco.get("rua").toString(),
                        endereco.get("numero").toString(),
                        endereco.get("cep").toString(),
                        endereco.get("complemento").toString()
                    );
                    contatoA.setEndereco(end);
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoA.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoA.getListaDeTelefones().add(tel);
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoA.getListaDeReunioes().add(reuniaoPOJO);
                    }              
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoA.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    Contatos.add(contatoA);
                
                }
                if(grupo.equals("Família")){
                    Familia contatoF = new Familia();
                    contatoF.setNome(nome);
                    contatoF.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoF.setGrupo(infoGrupo.get("parentesco").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    Endereco end = new Endereco(
                        endereco.get("pais").toString(),
                        endereco.get("estado").toString(),
                        endereco.get("cidade").toString(),
                        endereco.get("bairro").toString(),
                        endereco.get("rua").toString(),
                        endereco.get("numero").toString(),
                        endereco.get("cep").toString(),
                        endereco.get("complemento").toString()
                    );
                    contatoF.setEndereco(end);
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoF.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoF.getListaDeTelefones().add(tel);
                    }          
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoF.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    Contatos.add(contatoF);
                }
            }
            
        }catch(SQLException  | ParseException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
        return Contatos;
    }
    public List pesquisarContatosCompleto(int modoExibicao, String pesquisa){
        String where = "";
        String temPesquisa;
        if(modoExibicao == 1){where = " WHERE GRUPO = 'f'";}
        if(modoExibicao == 2){where = " WHERE GRUPO = 'a'";}
        if(modoExibicao == 3){where = " WHERE GRUPO = 't'";}
        
        List<Contato> Contatos = new ArrayList<>();
        try{
            
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("select * from CONTATOS"+ where +" order by NOME asc");
                      
            while(res.next()){
                temPesquisa = ":(";
                String nome = res.getString(1);
                String grupo = res.getString(2);
                switch(grupo){
                    case "t":
                        grupo = "Trabalho";
                    break;
                    case "a":
                        grupo = "Amigo";
                    break;
                    case "f":
                        grupo = "Família";
                    break;
                }
                if(grupo.equals("Família")){
                    Familia contatoF = new Familia();
                    contatoF.setNome(nome);
                    contatoF.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoF.setParentesco(infoGrupo.get("parentesco").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoF.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoF.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoF.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoF.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    if(contatoF.getNome().contains(pesquisa)){temPesquisa = ":)";}
                    for(int i = 0; i<contatoF.getListaDeEmails().size();i++){
                        if(contatoF.getListaDeEmails().get(i).contains(pesquisa)){temPesquisa = ":)";}
                    }
                    for(int i = 0; i<contatoF.getListaDeTelefones().size();i++){
                        if(contatoF.getListaDeTelefones().get(i).codigo.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoF.getListaDeTelefones().get(i).operadora.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoF.getListaDeTelefones().get(i).numero.contains(pesquisa)){temPesquisa = ":)";}
                    }
                    if(temPesquisa.equals(":)")){Contatos.add(contatoF);}
                    
                }
                if(grupo.equals("Amigo")){
                    Amigo contatoA = new Amigo();
                    contatoA.setNome(nome);
                    contatoA.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoA.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoA.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoA.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoA.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoA.getListaDeReunioes().add(reuniaoPOJO);
                    }
                    if(contatoA.getNome().contains(pesquisa)){temPesquisa = ":)";}
                    for(int i = 0; i<contatoA.getListaDeEmails().size();i++){
                        if(contatoA.getListaDeEmails().get(i).contains(pesquisa)){temPesquisa = ":)";}
                    }
                    for(int i = 0; i<contatoA.getListaDeTelefones().size();i++){
                        if(contatoA.getListaDeTelefones().get(i).codigo.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoA.getListaDeTelefones().get(i).operadora.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoA.getListaDeTelefones().get(i).numero.contains(pesquisa)){temPesquisa = ":)";}
                    }
                    if(temPesquisa.equals(":)")){Contatos.add(contatoA);}
                }
                if(grupo.equals("Trabalho")){
                    Trabalho contatoT = new Trabalho();
                    contatoT.setNome(nome);
                    contatoT.setGrupo(grupo);
                    JSONParser parser = new JSONParser();
                    JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                    contatoT.setEmpresa(infoGrupo.get("empresa").toString());
                    contatoT.setSetor(infoGrupo.get("setor").toString());
                    JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                    contatoT.setEndereco(
                        new Endereco(
                            endereco.get("pais").toString(),
                            endereco.get("estado").toString(),
                            endereco.get("cidade").toString(),
                            endereco.get("bairro").toString(),
                            endereco.get("rua").toString(),
                            endereco.get("numero").toString(),
                            endereco.get("cep").toString(),
                            endereco.get("complemento").toString()
                        )
                    );
                    JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                    contatoT.setNascimento(
                            new Data(
                                Integer.parseInt(nascimento.get("dia").toString()),
                                Integer.parseInt(nascimento.get("mes").toString()),
                                Integer.parseInt(nascimento.get("ano").toString())
                            )
                    );
                    JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                    JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                    for(int i = 0; i<listaDeTelefones.size(); i++){
                        JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                        Telefone tel = new Telefone(
                                telefone.get("operadora").toString(),
                                telefone.get("codigo").toString(),
                                telefone.get("numero").toString()
                        );
                        contatoT.getListaDeTelefones().add(tel);
                    }
                    JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                    JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                    for(int i = 0; i<listaDeEmails.size(); i++){
                        contatoT.getListaDeEmails().add(listaDeEmails.get(i).toString());
                    }
                    JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                    JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                    for(int i = 0; i<listaDeReunioes.size(); i++){
                        JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                        Reuniao reuniaoPOJO = new Reuniao(
                                reuniao.get("titulo").toString(),
                                new Data(reuniao.get("data").toString()),
                                reuniao.get("mensagem").toString()
                        );
                        contatoT.getListaDeReunioes().add(reuniaoPOJO);
                    }
                    if(contatoT.getNome().contains(pesquisa)){temPesquisa = ":)";}
                    for(int i = 0; i<contatoT.getListaDeEmails().size();i++){
                        if(contatoT.getListaDeEmails().get(i).contains(pesquisa)){temPesquisa = ":)";}
                    }
                    for(int i = 0; i<contatoT.getListaDeTelefones().size();i++){
                        if(contatoT.getListaDeTelefones().get(i).codigo.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoT.getListaDeTelefones().get(i).operadora.contains(pesquisa)){temPesquisa = ":)";}
                        if(contatoT.getListaDeTelefones().get(i).numero.contains(pesquisa)){temPesquisa = ":)";}
                    }
                    if(temPesquisa.equals(":)")){Contatos.add(contatoT);}
                }
            }
            
        }catch(SQLException  | ParseException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
        return Contatos;
    }
    
    public void atualizarTelefones(Contato contato){
        try{
            JSONArray JlistaDeTelefones = new JSONArray();  //ADICIONA OS QUE JA TINHA
            for(int i = 0; i< contato.getListaDeTelefones().size(); i++){
                JSONObject telefoneJSON = new JSONObject();
                telefoneJSON.put("operadora", contato.getListaDeTelefones().get(i).operadora);
                telefoneJSON.put("codigo", contato.getListaDeTelefones().get(i).codigo);
                telefoneJSON.put("numero", contato.getListaDeTelefones().get(i).numero);
                JlistaDeTelefones.add(telefoneJSON);
            }
            
            JSONObject telefones = new JSONObject();
            telefones.put("telefones",JlistaDeTelefones);
            
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET TELEFONES =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, telefones.toString());
            ps.setString(2, contato.getNome());
            ps.execute();
            ps.close();
            
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public void atualizarReunioes(Contato contato){
        try{
            JSONObject reunioes = new JSONObject();
            if(contato instanceof Trabalho){
                Trabalho contatoT = (Trabalho) contato;
                JSONArray JlistaDeReunioes = new JSONArray();  //ADICIONA OS QUE JA TINHA
                for(int i = 0; i< contatoT.getListaDeReunioes().size(); i++){
                    JSONObject reuniaoJSON = new JSONObject();
                    reuniaoJSON.put("titulo", contatoT.getListaDeReunioes().get(i).titulo);
                    reuniaoJSON.put("data", contatoT.getListaDeReunioes().get(i).data.toString());
                    reuniaoJSON.put("mensagem", contatoT.getListaDeReunioes().get(i).mensagem);
                    JlistaDeReunioes.add(reuniaoJSON);
                }
                reunioes = new JSONObject();
                reunioes.put("reunioes",JlistaDeReunioes);
            }
            if(contato instanceof Amigo){
                Amigo contatoA = (Amigo) contato;
                JSONArray JlistaDeReunioes = new JSONArray();  //ADICIONA OS QUE JA TINHA
                for(int i = 0; i< contatoA.getListaDeReunioes().size(); i++){
                    JSONObject reuniaoJSON = new JSONObject();
                    reuniaoJSON.put("titulo", contatoA.getListaDeReunioes().get(i).titulo);
                    reuniaoJSON.put("data", contatoA.getListaDeReunioes().get(i).data.toString());
                    reuniaoJSON.put("mensagem", contatoA.getListaDeReunioes().get(i).mensagem);
                    JlistaDeReunioes.add(reuniaoJSON);
                }
                reunioes = new JSONObject();
                reunioes.put("reunioes",JlistaDeReunioes);
            }
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET REUNIOES =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, reunioes.toString());
            ps.setString(2, contato.getNome());
            ps.execute();
            ps.close();
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public void atualizarEmails(Contato contato){
        try{
            JSONArray JlistaDeEmails = new JSONArray();
            for(int i = 0; i< contato.getListaDeEmails().size(); i++){
                JlistaDeEmails.add(contato.getListaDeEmails().get(i));
            }
            JSONObject emails = new JSONObject();
            emails.put("emails", JlistaDeEmails);
            
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET EMAILS =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, emails.toString());
            ps.setString(2, contato.getNome());
            ps.execute();
            ps.close();
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
}
