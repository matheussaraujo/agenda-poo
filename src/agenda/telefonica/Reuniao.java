package agenda.telefonica;
/**
 * @author Marcos Junior
 */
public class Reuniao {
    String titulo;
    Data data;
    String mensagem;
    Reuniao(String titulo, Data data, String mensagem){
        this.titulo = titulo;
        this.data = data;
        this.mensagem = mensagem;
    }
}
