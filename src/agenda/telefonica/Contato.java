package agenda.telefonica;
/**
 * @author Marcos Junior
*/

import java.util.List;

public abstract class Contato {
    protected String nome, grupo;
    protected Endereco endereco;
    protected Data nascimento;
    protected List<Telefone> listaDeTelefones;
    protected List<String> listaDeEmails;

    public void setAniversario(int dia, int mes, int ano) {
        this.nascimento.dia = dia;
        this.nascimento.mes = mes;
        this.nascimento.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Data getNascimento() {
        return nascimento;
    }

    public void setNascimento(Data nascimento) {
        this.nascimento = nascimento;
    }

    public List<Telefone> getListaDeTelefones() {
        return listaDeTelefones;
    }

    public void setListaDeTelefones(List<Telefone> listaDeTelefones) {
        this.listaDeTelefones = listaDeTelefones;
    }

    public List<String> getListaDeEmails() {
        return listaDeEmails;
    }

    public void setListaDeEmails(List<String> listaDeEmails) {
        this.listaDeEmails = listaDeEmails;
    }
    
}
